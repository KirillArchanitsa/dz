package ru.sberbank;


import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;

public class MyStack <E extends Comparable<? super E>>{
    private int maxSize;
    private List<E> stackArray;
    private int top;
    private E minElem;


    public MyStack(int s) {
        maxSize = s;
        stackArray = new ArrayList<>(s);
        top = 0;
    }
    public void addElem(E j) {
        if(top > 0){
            if(j.compareTo(minElem) < 0){
                minElem = j;
            }
        }
        else{
            minElem = j;
        }
        stackArray.add(j);
        ++top;


    }
    public E removeElem() throws EmptyStackException {
        if (maxSize <= 0){
            throw new EmptyStackException();
        }
        return stackArray.remove(--top);

    }

    public E minElem() throws EmptyStackException{
            if(top >= 0)
                return minElem;
            else {
                throw new EmptyStackException();
            }
    }

}