package ru.sbt.lesson6;

public class GoWalkException extends Exception{

    public GoWalkException(String message) {
        super(message);
    }
}
