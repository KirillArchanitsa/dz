package ru.sbt.lesson6;

public class Dog extends Animal{
    private boolean muzzleDressed = false;

    public Dog(String name, Gender genderOfAnimal) {
        super(name, genderOfAnimal);
    }
    public Dog setMuzzleDressed(boolean muzzleDressed) {
        this.muzzleDressed = muzzleDressed;
        return this;
    }

    public boolean isMuzzleDressed() {
        return muzzleDressed;
    }

    void dressMuzzle(){
        muzzleDressed = true;
    }

    void pullOffMuzzle(){
        muzzleDressed = false;
    }

    void bark(){
        System.out.println("gav gav.");
    }

    @Override
    public void goWalk() throws GoWalkException{
        if (muzzleDressed){
            System.out.printf("%s muzzle dressed.\n", super.getName());
        }
        else{
            throw new HaveMuzzleException(String.format("Muzzle %s is not dressed, stay home!", super.getName()));
        }
        super.goWalk();
    }

}
