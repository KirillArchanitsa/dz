package ru.sbt.lesson6;

public abstract class Animal implements Pet{
    private String name;
    private Gender genderOfAnimal;
    private boolean isSleep = false;

    public Animal(String name, Gender genderOfAnimal) {
        this.name = name;
        this.genderOfAnimal = genderOfAnimal;
    }



    public String getName() {
        return name;
    }

    public Animal setName(String name) {
        this.name = name;
        return this;
    }

    public Gender getGenderOfAnimal() {
        return genderOfAnimal;
    }

    public Animal setGenderOfAnimal(Gender genderOfAnimal) {
        this.genderOfAnimal = genderOfAnimal;
        return this;
    }
    void goSleep(){
        isSleep = true;
    }

    void wakeUp(){
        isSleep = false;
    }

    @Override
    public String toString() {
        return "Animal name is " + name + "\n";
    }

    public void goWalk() throws GoWalkException{
        if (isSleep){
            throw new GoWalkException(String.format("%s is sleep! Stay home", name));
        }
        else{
            System.out.printf("%s go walk.\n", name);
        }
    }

}
