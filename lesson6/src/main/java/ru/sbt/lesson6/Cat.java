package ru.sbt.lesson6;

public class Cat extends Animal{
    public Cat(String name, Gender genderOfAnimal) {
        super(name, genderOfAnimal);
    }

    void meow(){
        System.out.println("meow meow");
    }

}
