package ru.sbt.lesson6;

public class HaveMuzzleException extends GoWalkException{

    public HaveMuzzleException(String message) {
        super(message);
    }
}
