package ru.sbt.lesson6;

public class MainWork {
    public static void main(String[] args) {
        Cat myCat = new Cat("Scott", Gender.MALE);
        Dog myDog = new Dog("Cate", Gender.FEMALE);

        //Normal work
        myCat.wakeUp();
        try{
            myCat.goWalk();
        }
        catch (GoWalkException ex){
            System.out.println(ex.getMessage());
        }

        myDog.wakeUp();
        myDog.dressMuzzle();
        try{
            myDog.goWalk();
        }
        catch (GoWalkException ex){
            System.out.println(ex.getMessage());
        }

        System.out.println("\n");


        //Problem
        myCat.goSleep();
        try{
            myCat.goWalk();
        }
        catch (GoWalkException ex){
            System.out.println(ex.getMessage());
        }

        myDog.pullOffMuzzle();
        try{
            myDog.goWalk();
        }
        catch (HaveMuzzleException ex){
            System.out.println(ex.getMessage());
        }
        catch (GoWalkException ex){
            System.out.println(ex.getMessage());
        }
    }
}
