package ru.sber.jd;

public class Lambda {
    private int x;
    private int y;
    private String value;

    public String getValue() {
        return value;
    }

    public Lambda setValue(String value) {
        this.value = value;
        return this;
    }


    public Lambda(int x, int y, String value) {
        this.x = x;
        this.y = y;
        this.value = value;
    }

    public int getX() {
        return x;
    }

    public Lambda setX(int x) {
        this.x = x;
        return this;
    }

    public int getY() {
        return y;
    }

    public Lambda setY(int y) {
        this.y = y;
        return this;
    }

    @Override
    public String toString() {
        return "lambda{" +
                "x=" + x +
                ", y=" + y +
                ", value='" + value + '\'' +
                '}';
    }
}
