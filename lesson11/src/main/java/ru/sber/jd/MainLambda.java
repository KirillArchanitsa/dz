package ru.sber.jd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainLambda {
    public static void main(String[] args) {

        List <Lambda> myLambdas = new ArrayList<>();
        for (int i = 0; i < 9;i++) {
            myLambdas.add(new Lambda(i, i, "Lambada_" + i));
        }

        Map <Integer, Map<Integer, String>>  mainMap= new HashMap<>();
        myLambdas.forEach(e -> {
            Map<Integer, String> tmpMap = new HashMap<>();
            tmpMap.put(e.getY(), e.getValue());
            mainMap.put(e.getX(),tmpMap);
        });

        System.out.println("Result:");
        mainMap.forEach((k,v) -> System.out.println("Key = " + k + "\tValue = " + v));
    }
}
