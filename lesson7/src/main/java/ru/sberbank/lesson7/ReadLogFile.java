package ru.sberbank.lesson7;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class ReadLogFile {
    void readLogFile(String path ){
        try {
            List<String> content = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
            System.out.println(content);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
