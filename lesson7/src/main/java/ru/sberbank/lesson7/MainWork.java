package ru.sberbank.lesson7;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class MainWork {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {

        logger.info("First Error");
        try {
            CreateFirstError stackOverflowError = new CreateFirstError();
            stackOverflowError.getStackOverflow(1);
        }
        catch (Error error){
            logger.error("StackOverflowError ", error);
        }

        logger.info("Second Error");
        try{
            CreateSecondError outOfMemoryError= new CreateSecondError();
            String path = "E:\\T2.SkyNet.Edition.1080p\\T2.SkyNet.Edition.BDRip.1080p.Rus.mkv";
            outOfMemoryError.getOutOfMemoryError(path);
        }
        catch (Error error){
            logger.error("OutOfMemoryError ", error);
        }

        logger.info("Third Error");
        List <CreateSecondError> list = new ArrayList<>();
        try{
            while(true){
                list.add(new CreateSecondError());
            }
        }
        catch (Error error){
            logger.error("OutOfMemoryError ", error);
        }

        logger.info("Finish\n Read log file to console");
        ReadLogFile myReader = new ReadLogFile();
        myReader.readLogFile("logs/log4j2.log");
    }
}
