package ru.sberbank.lesson7;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CreateSecondError {

    void getOutOfMemoryError (String path ){
        try {

            // default StandardCharsets.UTF_8
            byte[] byteData= Files.readAllBytes(Paths.get(path));
            for(byte b: byteData)
                System.out.print(b);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
