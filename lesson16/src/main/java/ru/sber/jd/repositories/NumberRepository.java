package ru.sber.jd.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.sber.jd.entity.NumberEntity;

@Repository
public interface NumberRepository extends CrudRepository<NumberEntity, Integer> {
}
