package ru.sber.jd.services;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import ru.sber.jd.entity.NumberEntity;
import ru.sber.jd.repositories.NumberRepository;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Properties;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class NumberService implements CommandLineRunner {
    private final static String TOPIC="kafka-lesson-topic";
    private final static String BOOTSTRAP_SERVER = "localhost:9092";
    private final NumberRepository numberRepository;

    @Override
    public void run(String... args) throws Exception {

        Thread producerThread = new Thread(() -> {
            Random randomForStr = new Random();
            final Producer<String, String> producer = createProducer();
            while (true) {
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                byte[] array = new byte[8];
                randomForStr.nextBytes(array);
                String generatedString = new String(array, StandardCharsets.UTF_8);

                final ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC, "1", generatedString);
                try {
                    RecordMetadata metadata = producer.send(record).get();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        producerThread.start();


        Thread consumerThread = new Thread(() -> {
            final Consumer<String, String> consumer = createConsumer();
            while (true) {
                final ConsumerRecords<String, String> consumerRecords = consumer.poll(1000);

                if (consumerRecords.count()==0) {
                    continue;
                }
                consumerRecords.forEach(record -> {
                    NumberEntity numberEntity = new NumberEntity();
                    numberEntity.setStrFromKafaka(record.value());
                    numberRepository.save(numberEntity);
                    System.out.println("\n\nAll values = "+ numberRepository.findAll());
                });
                consumer.commitAsync();
            }
        });
        consumerThread.start();

    }

    private static Consumer<String, String> createConsumer() {
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVER);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "KafkaConsumer");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        final Consumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(TOPIC));
        return consumer;
    }


    private static Producer<String, String> createProducer() {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                BOOTSTRAP_SERVER);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaProducer");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class.getName());
        return new KafkaProducer<>(props);
    }

}
