package ru.sber.jd.lesson14.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.jd.lesson14.enteries.EmployeesEntity;

@Repository
public interface EmployeesRepositories  extends JpaRepository<EmployeesEntity, Integer> {
}
