package ru.sber.jd.lesson14.enteries;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;


import javax.persistence.*;
import java.io.Serializable;


@Data
@Entity(name = "EmployeesSalary")
public class EmployeesSalaryEntity implements Serializable {

    @ManyToOne
    @JoinColumn
    private EmployeesEntity empid;
    @Column
    private Integer salary;
    @Id
    @Column
    @GenericGenerator(name="generator", strategy = "increment")
    @GeneratedValue(generator="generator")
    private Integer id;
}
