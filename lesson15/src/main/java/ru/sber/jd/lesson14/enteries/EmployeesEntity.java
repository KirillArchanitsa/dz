package ru.sber.jd.lesson14.enteries;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity(name = "Employees")
public class EmployeesEntity {
    @Id
    @Column
    @GenericGenerator(name="generator", strategy = "increment")
    @GeneratedValue(generator="generator")
    private Integer empid;
    @Column
    private String firstname;
    @Column
    private String lastname;
}
