package ru.sber.jd.lesson14.services;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import ru.sber.jd.lesson14.enteries.EmployeesEntity;
import ru.sber.jd.lesson14.enteries.EmployeesSalaryEntity;
import ru.sber.jd.lesson14.repositories.EmployeesRepositories;
import ru.sber.jd.lesson14.repositories.EmployeesSalaryRepositories;

@Service
@RequiredArgsConstructor
public class TestServices implements CommandLineRunner {

    private final EmployeesRepositories employeesEntity;
    private final EmployeesSalaryRepositories employeesSalaryRepositories;

    @Override
    public void run(String... args) throws Exception {
        EmployeesEntity myEmployeesEntity = new EmployeesEntity();
        EmployeesSalaryEntity myEmployeesSalaryEntity = new EmployeesSalaryEntity();

        //myEmployeesEntity.setEmpid(1);
        myEmployeesEntity.setFirstname("User1");
        myEmployeesEntity.setLastname("User1Userovich");
        EmployeesEntity myEmployeesEntitySave = employeesEntity.save(myEmployeesEntity);

        EmployeesEntity ee = new EmployeesEntity();
        ee.setEmpid(myEmployeesEntitySave.getEmpid());
        myEmployeesSalaryEntity.setEmpid(ee);
        //myEmployeesSalaryEntity.setId(1);
        myEmployeesSalaryEntity.setSalary(200);
        employeesSalaryRepositories.save(myEmployeesSalaryEntity);

        System.out.println(employeesEntity.findAll());
        System.out.println(employeesSalaryRepositories.findAll());

    }
}
