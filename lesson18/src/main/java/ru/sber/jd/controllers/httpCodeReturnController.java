package ru.sber.jd.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.jd.exceptions.exampleHttpExecption;
import java.util.Arrays;


@RestController
public class httpCodeReturnController {


    @GetMapping("testHttpResponse/{codeStr}")
    public ResponseEntity<String> getHttpCode(@PathVariable Integer codeStr) throws exampleHttpExecption {
       if (Arrays.stream(HttpStatus.values()).anyMatch(e -> codeStr == e.value())){
            if ((codeStr / 100 == 4) || (codeStr / 100 == 5)) {
                throw new exampleHttpExecption(codeStr);
            }
            else {
                return ResponseEntity.ok("You enter - " + HttpStatus.valueOf(codeStr));
            }
        }
        else{
            return ResponseEntity.badRequest().build();
        }

    }


    @ExceptionHandler(exampleHttpExecption.class)
    public ResponseEntity <String>fail(exampleHttpExecption ex){
        return ResponseEntity.badRequest().body("You enter ERROR HTTP code - " + HttpStatus.valueOf(ex.getHttCode()).toString());
    }

}
