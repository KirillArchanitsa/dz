package ru.sber.lesson13;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Sql {
    Connection connect;

    public Sql(Connection connect){
        this.connect = connect;
    }

    public void createTable() throws SQLException {
        Statement statement = connect.createStatement();
        statement.execute("CREATE TABLE IF NOT EXISTS Employees " +
                "(empid INTEGER, firstname TEXT, lastname TEXT)");
        statement.execute("CREATE TABLE IF NOT EXISTS EmployeesSalary " +
                "(empid INTEGER, salary INTEGER)");
        connect.commit();
    }

    public void selectTable(String sql) throws SQLException{
        Statement statement = connect.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        while(resultSet.next()){
            System.out.println(
                    "empid - " + resultSet.getString ("empid") +
                            " firstname - " + resultSet.getString("firstname") +
                  " lastname -" + resultSet.getString("lastname")+
                  " salary - " + resultSet.getInt("salary"));
        }
    }

    public void inserTable() throws SQLException{
        Statement statement = connect.createStatement();
        statement.execute("insert into Employees(empid, firstname, lastname) " +
                "values (1,'FirstUser','Userovich'), "+
                "(2,'SecondtUser','Userovich2'), " + "(3,'ThirdUser','Userovich3')" );

        statement.execute("insert into EmployeesSalary(empid, salary) " +
                "values (1,10), "+
                "(2,20)");
        connect.commit();
    }

}
