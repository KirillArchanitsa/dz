package ru.sber.lesson13;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class MainWork {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        final String SQL_INNER_JOIN = "select * from Employees as e  INNER JOIN EmployeesSalary as s on e.empid = s.empid";
        final String SQL_LEFT_JOIN = "select * from Employees as e LEFT JOIN EmployeesSalary as s  on e.empid = s.empid";
        final String SQL_RIGHT_JOIN = "select * from Employees as e RIGHT JOIN EmployeesSalary as s on e.empid = s.empid";
        final String SQL_OUTER_JOIN = "select * from Employees as e FULL OUTER JOIN EmployeesSalary as s on e.empid = s.empid";

        Connection connect = DriverManager.getConnection("jdbc:h2:~/test","sa","");
        Sql sqlExample = new Sql(connect);
        sqlExample.createTable();
        sqlExample.inserTable();
        sqlExample.selectTable(SQL_INNER_JOIN);
        sqlExample.selectTable(SQL_LEFT_JOIN);
        sqlExample.selectTable(SQL_RIGHT_JOIN);

        connect.close();
    }

}
