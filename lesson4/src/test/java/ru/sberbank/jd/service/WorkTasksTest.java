package ru.sberbank.jd.service;

import org.junit.Assert;
import org.junit.Test;
import ru.sberbank.jd.dto.JiraTask;


public class WorkTasksTest {
    @Test
    public void openJiraTask(){
        JiraTask myJiraTask = new JiraTask("Java", "JD");
        Assert.assertTrue(myJiraTask.getJiraTaskIsOpen());
    }
    @Test
    public void closeJiraTask(){
        JiraTask myJiraTask = new JiraTask("Java", "JD");
        myJiraTask.closeJiraTask();
        Assert.assertFalse(myJiraTask.getJiraTaskIsOpen());
    }
}