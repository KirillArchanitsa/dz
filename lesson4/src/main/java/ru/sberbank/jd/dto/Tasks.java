package ru.sberbank.jd.dto;

public abstract class Tasks {

    private String name;
    private PriorityType priority;
    private String TextTask;
    private String Executor;

    public String getAuthor() {
        return Author;
    }

    public Tasks setAuthor(String author) {
        Author = author;
        return this;
    }

    private String Author;


    public String getExecutor() {
        return Executor;
    }

    public Tasks setExecutor(String name) {
        this.Executor = Executor;
        return this;
    }

    public String getName() {
        return name;
    }

    public Tasks setName(String name) {
        this.name = name;
        return this;
    }

    public PriorityType getPriority() {
        return priority;
    }

    public Tasks setPriority(PriorityType priority) {
        this.priority = priority;
        return this;
    }

    public String getTextTask() {
        return TextTask;
    }

    public Tasks setTextTask(String textTask) {
        TextTask = textTask;
        return this;
    }

    @Override
    public String toString() {
        return "Tasks name " + name;
    }


}
