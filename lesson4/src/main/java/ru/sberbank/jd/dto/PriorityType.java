package ru.sberbank.jd.dto;

public enum PriorityType {
    LOW,
    MIDDLE,
    HIGHT
}
