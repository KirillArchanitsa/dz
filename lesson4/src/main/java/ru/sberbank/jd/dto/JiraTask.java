package ru.sberbank.jd.dto;

public class JiraTask extends Tasks{
    private String Component;
    private String Project;
    private boolean JiraTaskIsOpen;

    public boolean isJiraTaskIsOpen() {
        return JiraTaskIsOpen;
    }


    public JiraTask(String Component, String Project){
        this.Component = Component;
        this.Project = Project;
        JiraTaskIsOpen = true;
    }

    public void closeJiraTask(){
        JiraTaskIsOpen = false;
    }

    public boolean getJiraTaskIsOpen() {
        return JiraTaskIsOpen;
    }


    public String getComponent() {
        return Component;
    }

    public JiraTask setComponent(String component) {
        Component = component;
        return this;
    }

    public String getProject() {
        return Project;
    }

    public JiraTask setProject(String project) {
        Project = project;
        return this;
    }



}
