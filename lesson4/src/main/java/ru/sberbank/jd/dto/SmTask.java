package ru.sberbank.jd.dto;

public class SmTask extends Tasks {
    private String ServiceName;
    private boolean SmTaskIsOpen;

    public SmTask(String ServiceName){
        this.ServiceName = ServiceName;
        SmTaskIsOpen = true;
    }

    public String getObject() {
        return ServiceName;
    }

    public SmTask setObject(String object) {
        ServiceName = object;
        return this;
    }

    void closeSmTask(){
        SmTaskIsOpen = false;
    }

    public boolean isSmTaskIsOpen() {
        return SmTaskIsOpen;
    }
}
