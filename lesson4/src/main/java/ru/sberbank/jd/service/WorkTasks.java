package ru.sberbank.jd.service;

import ru.sberbank.jd.dto.JiraTask;

public class WorkTasks {
    public static void main(String[] args) {
        JiraTask myJiraTask = new JiraTask("Java", "JD");
        System.out.println(myJiraTask.getJiraTaskIsOpen());
        myJiraTask.closeJiraTask();
        System.out.println(myJiraTask.getJiraTaskIsOpen());
    }
}
