package ru.sberbank;

public class MyHashMapTest {
    public static void main(String[] args) {
        MyHashMap <String, Integer> hm = new MyHashMap<>();
        hm.add("1", 1);
        hm.add("2", 2);

        System.out.println(hm.get("2"));
        System.out.println(hm.get("3"));
        System.out.println(hm.contains("1"));
        System.out.println(hm.contains("3"));

        //Check type
        //hm.add(0, 2);


    }
}
