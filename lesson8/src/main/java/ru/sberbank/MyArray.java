package ru.sberbank;

import java.util.Arrays;

public class MyArray<E> {

    private E[] myarray;
    private int nextlndex = 0;

    public MyArray() {
        this.myarray = (E[]) new Object[10];
    }

    public int size() {
        return this.nextlndex;
    }

    public void add(E d) {
        System.out.println(d.getClass().getName());
        if (nextlndex < myarray.length) {
            myarray[nextlndex] = d;
            ++nextlndex;
        }
        else{
            myarray = Arrays.copyOf(myarray, nextlndex + 10);
            myarray[++nextlndex] = d;
        }
    }

    public void remove( int e){
        if (nextlndex < e)
            throw new RuntimeException("You try remove element number " + e + ". Max element is  " +  nextlndex);
        else if (e < 0)
            throw new RuntimeException("You try remove element number " + e + ". Elem must be more than 0, current max element is  " +  nextlndex);
        else{
            myarray[e] = null;
        }
    }


    public E get(int index) {
        if (index >= nextlndex || index < 0){
            throw new IndexOutOfBoundsException("You enter index - " + index + ",Current size " + nextlndex);
        }
        return myarray[index];
    }

}