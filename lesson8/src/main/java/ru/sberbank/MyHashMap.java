package ru.sberbank;


import java.util.ArrayList;
import java.util.List;


public class MyHashMap<K,V> {

    class MyMap {
        K key;
        V value;

        public void insert(K k, V v) {
            this.key = k;
            this.value =v;
        }
    }

    private MyMap map;
    private List<MyMap> recordList;

    public MyHashMap() {
        this.recordList = new ArrayList<MyMap>();
    }

    public boolean contains(K k) {
        for (int i = 0; i < recordList.size(); i++) {
            MyMap myMap = recordList.get(i);
            if (myMap.key.equals(k)) {
                return true;
            }
        }
        return false;
    }


    public void add(K k, V v) {
        this.map = new MyMap();
        map.insert(k, v);

        for (int i = 0; i < recordList.size(); i++) {
            MyMap myMap = recordList.get(i);
            if (myMap.key.equals(k)) {
                recordList.remove(i);
                break;
            }
        }
        recordList.add(map);
    }

    public V get(K key) {
        for (int i = 0; i < this.recordList.size(); i++) {
            MyMap con = recordList.get(i);
            if (key.toString() == con.key.toString()) {
                return con.value;
            }

        }
        return null;
    }
}

