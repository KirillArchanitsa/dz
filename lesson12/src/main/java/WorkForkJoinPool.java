import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class WorkForkJoinPool extends RecursiveTask<Integer> {
    private List<Integer> values;

    public WorkForkJoinPool(List<Integer> values){
        this.values = values;
    }

    @Override
    protected Integer compute() {
        if(values.size() ==1){
            return values.get(0);
        }
        List<Integer> firstPart = new ArrayList<>();
        List<Integer> secondPart = new ArrayList<>();;
        int mid = values.size()/2;

        for(int i = 0; i< mid; i++){
            firstPart.add(values.get(i));
        }
        for(int i = mid; i< values.size(); i++){
            secondPart.add(values.get(i));
        }
        WorkForkJoinPool left = new WorkForkJoinPool(firstPart);
        WorkForkJoinPool right = new WorkForkJoinPool(secondPart);
        left.fork();
        right.fork();
        return Calculator.getSum(right.join(), left.join());

    }


    public static void main(String[] args) {
        int result = 0;
        List<Integer> myNum = new ArrayList<>(1000);
        for (int i =1 ;i <= 1000 ;i ++)
            myNum.add(i);
        long start = System.currentTimeMillis();

        result = new ForkJoinPool(100).invoke(new WorkForkJoinPool(myNum));

        long end = System.currentTimeMillis();
        System.out.println("Result = " + result);
        float sec = (end - start) / 1000F;
        System.out.println(sec + " seconds");


        //Result = 500500 186.1 seconds default thread
        //Result = 500500 20.02 seconds 100 thread
    }
}
