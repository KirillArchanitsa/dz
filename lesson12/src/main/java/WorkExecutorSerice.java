import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class WorkExecutorSerice {

    public static void main(String[] args)  throws InterruptedException, ExecutionException {
        int result = 0;
        int numOfThread = Runtime.getRuntime().availableProcessors();

        long start = System.currentTimeMillis();
        ExecutorService executorService = Executors.newFixedThreadPool(numOfThread);
        for (final int[] i = {1}; i[0] <= 1000 ; i[0]++) {
            Future<Integer> future = executorService.submit(() -> {
                //System.out.println(Thread.currentThread().getName());
                return Calculator.getSum(i[0], ++i[0]);
            });
            result += future.get();
        }
        executorService.shutdown();
        long end = System.currentTimeMillis();

        System.out.println("Result = " + result);
        float sec = (end - start) / 1000F;
        System.out.println(sec + " seconds");


        //Result = 500500  500.298 seconds
        //Result = 500500 500.33 seconds 100 thread
    }
}
