import java.util.stream.IntStream;
import java.util.stream.Stream;

public class WorkPStream {
    static volatile int result;
    public static void main(String[] args) {
        Stream<Integer> stream  = IntStream.range(1, 1001).boxed();
        long start = System.currentTimeMillis();
        Integer sum = stream.parallel().reduce(0, Calculator::getSum);
        long end = System.currentTimeMillis();

        System.out.println("Result = " + sum);
        float sec = (end - start) / 1000F;
        System.out.println(sec + " seconds");

        //196.131 seconds 500500

    }
}
