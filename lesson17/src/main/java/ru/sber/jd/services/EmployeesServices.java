package ru.sber.jd.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sber.jd.dto.EmployeesEntityDto;
import ru.sber.jd.enteries.EmployeesEntity;
import ru.sber.jd.repositories.EmployeesRepositories;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


@Service
@RequiredArgsConstructor
public class EmployeesServices {

    private final EmployeesRepositories employeesRepositories;


    public EmployeesEntityDto save(EmployeesEntityDto dto){
        EmployeesEntity employeesEntity = mapToEntity(dto);
        EmployeesEntity savedEntity = employeesRepositories.save(employeesEntity);
        return mapToDto(savedEntity);
    }


    public List<EmployeesEntityDto> getAll(){
        return StreamSupport.stream(employeesRepositories.findAll().spliterator(), false)
                .map(this::mapToDto)
                .collect(Collectors.toList());
    }

    public void delete(Integer id){
        employeesRepositories.deleteById(id);
    }

    public EmployeesEntityDto getById(Integer id){
        return mapToDto(employeesRepositories.findById(id).orElse(new EmployeesEntity()));
    }

    public void change(EmployeesEntityDto dto){
        if(employeesRepositories.existsById(dto.getEmpid())){
            save(dto);
        }
    }

    private EmployeesEntity mapToEntity(EmployeesEntityDto dto){
        EmployeesEntity employeesEntity = new EmployeesEntity();
        employeesEntity.setEmpid(dto.getEmpid());
        employeesEntity.setFirstname(dto.getFirstname());
        employeesEntity.setLastname(dto.getLastname());
        return employeesEntity;

    }

    private EmployeesEntityDto mapToDto(EmployeesEntity employeesEntity){
        EmployeesEntityDto dto = new EmployeesEntityDto();
        dto.setEmpid(employeesEntity.getEmpid());
        dto.setFirstname(employeesEntity.getFirstname());
        dto.setLastname(employeesEntity.getLastname());
        return dto;
    }

}