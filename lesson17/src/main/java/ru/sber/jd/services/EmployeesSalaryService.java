package ru.sber.jd.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.sber.jd.dto.EmployeesSalaryEntityDto;
import ru.sber.jd.enteries.EmployeesEntity;
import ru.sber.jd.enteries.EmployeesSalaryEntity;
import ru.sber.jd.repositories.EmployeesSalaryRepositories;
import javax.xml.ws.http.HTTPException;
import java.util.List;

import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class EmployeesSalaryService {

    private final EmployeesSalaryRepositories employeesSalaryRepositories;


    public EmployeesSalaryEntityDto save(EmployeesSalaryEntityDto employeesSalaryEntityDto){
        EmployeesSalaryEntity employeesSalaryEntity = mapToEntity(employeesSalaryEntityDto);
        EmployeesSalaryEntity savedEmployeesSalaryEntity = employeesSalaryRepositories.save(employeesSalaryEntity);
        return mapToDto(savedEmployeesSalaryEntity);
    }

    public EmployeesSalaryEntityDto getById(Integer id){
        return mapToDto(employeesSalaryRepositories.findById(id).orElse(new EmployeesSalaryEntity()));
    }

    public void change(EmployeesSalaryEntityDto employeesSalaryEntityDto) {
        employeesSalaryRepositories.findById(employeesSalaryEntityDto.getId()).ifPresent( e ->{
            e.setSalary(employeesSalaryEntityDto.getSalary());
            employeesSalaryRepositories.save(e);
        });
    }


    public void delete(Integer id){
        employeesSalaryRepositories.deleteById(id);
    }

    public List<EmployeesSalaryEntityDto> getAll(){
        return employeesSalaryRepositories.findAll().stream()
                .map(this::mapToDto)
                .collect(Collectors.toList());
    }



    private EmployeesSalaryEntity mapToEntity(EmployeesSalaryEntityDto dto){
        EmployeesSalaryEntity employeesSalaryEntity = new EmployeesSalaryEntity();

        EmployeesEntity employeesEntity = new EmployeesEntity();
        employeesEntity.setEmpid(dto.getEmpid());

        employeesSalaryEntity.setEmpid(employeesEntity);
        employeesSalaryEntity.setId(dto.getId());
        employeesSalaryEntity.setSalary(dto.getSalary());
        return employeesSalaryEntity;
    }

    private EmployeesSalaryEntityDto mapToDto(EmployeesSalaryEntity employeesSalaryEntity){
        EmployeesSalaryEntityDto employeesSalaryEntityDto = new EmployeesSalaryEntityDto();

        employeesSalaryEntityDto.setEmpid(employeesSalaryEntity.getEmpid().getEmpid());
        employeesSalaryEntityDto.setId(employeesSalaryEntity.getId());
        employeesSalaryEntityDto.setSalary(employeesSalaryEntity.getSalary());
        return employeesSalaryEntityDto;
    }


}
