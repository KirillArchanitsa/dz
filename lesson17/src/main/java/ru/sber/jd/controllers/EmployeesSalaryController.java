package ru.sber.jd.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.sber.jd.dto.EmployeesSalaryEntityDto;
import ru.sber.jd.services.EmployeesSalaryService;

import java.util.List;


@RestController
@RequiredArgsConstructor
public class EmployeesSalaryController {
    private final EmployeesSalaryService employeesSalaryService;

    @PostMapping("/EmployeesSalary")
    public EmployeesSalaryEntityDto save(@RequestBody EmployeesSalaryEntityDto employeesSalaryEntityDto){
        return employeesSalaryService.save(employeesSalaryEntityDto);
    }

    @GetMapping("/EmployeesSalary")
    public List<EmployeesSalaryEntityDto> findAll(){
        return employeesSalaryService.getAll();
    }

    @GetMapping("/EmployeesSalary/{id}")
    public EmployeesSalaryEntityDto findById(@PathVariable Integer id){
        return employeesSalaryService.getById(id);
    }

    @DeleteMapping("/EmployeesSalary/{id}")
    public void delete(@PathVariable Integer id){
        employeesSalaryService.delete(id);
    }

    @PutMapping("/EmployeesSalary")
    public void put(@RequestBody EmployeesSalaryEntityDto employeesSalaryEntityDto){
        employeesSalaryService.change(employeesSalaryEntityDto);
    }
}
