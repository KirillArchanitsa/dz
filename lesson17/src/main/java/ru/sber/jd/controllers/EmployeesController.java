package ru.sber.jd.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.sber.jd.dto.EmployeesEntityDto;
import ru.sber.jd.services.EmployeesServices;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class EmployeesController {

    private final EmployeesServices employeesServices;

    @PostMapping("/Employees")
    public EmployeesEntityDto save(@RequestBody EmployeesEntityDto employeesEntityDto){
        return employeesServices.save(employeesEntityDto);
    }

    @GetMapping("/Employees")
    public List<EmployeesEntityDto> findAll(){
        return employeesServices.getAll();
    }

    @GetMapping("/Employees/{id}")
    public EmployeesEntityDto findById(@PathVariable Integer id){
        return employeesServices.getById(id);
    }

    @DeleteMapping("/Employees/{id}")
    public void delete(@PathVariable Integer id){
        employeesServices.delete(id);
    }

    @PutMapping("/Employees")
    public void put(@RequestBody EmployeesEntityDto employeesEntityDto){
        employeesServices.change(employeesEntityDto);
    }

}
