package ru.sber.jd.enteries;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;



@Data
@Entity(name = "EmployeesSalary")
public class EmployeesSalaryEntity  {

    @Id
    @Column
    @GenericGenerator(name="generator", strategy = "increment")
    @GeneratedValue(generator="generator")
    private Integer id;
    @ManyToOne
    @JoinColumn(name="Employees", referencedColumnName="empid")
    private EmployeesEntity empid;
    @Column
    private Integer salary;

}
