package ru.sber.jd.dto;

import lombok.Data;


@Data
public class EmployeesSalaryEntityDto {
    private Integer empid;
    private Integer salary;
    private Integer id;
}
