package ru.sber.jd.dto;

import lombok.Data;

@Data
public class EmployeesEntityDto {

    private Integer empid;
    private String firstname;
    private String lastname;
}
