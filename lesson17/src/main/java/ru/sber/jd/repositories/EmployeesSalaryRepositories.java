package ru.sber.jd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.jd.enteries.EmployeesEntity;
import ru.sber.jd.enteries.EmployeesSalaryEntity;


@Repository
public interface EmployeesSalaryRepositories extends JpaRepository<EmployeesSalaryEntity, Integer> {

}
