package ru.sber.jd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.jd.enteries.EmployeesEntity;


@Repository
public interface EmployeesRepositories  extends JpaRepository<EmployeesEntity, Integer> {
}
